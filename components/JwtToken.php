<?php

namespace app\components;
use Yii;
class JwtToken extends \yii\base\Component
{
    //время жизни access токена
    protected $access_expire = 8; //часов
    //время жизни refresh токена
    protected $refresh_expire = 30; //дней

    public function createToken($user)
    {
        if (empty($user->id)) return null;
        /** @var Jwt $jwt */
        $jwt = \Yii::$app->jwt;
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        $time = time();
        $return = [];
        $return['access_expire'] = $time + 3600 * $this->access_expire;
        $return['access_token'] = (string)$jwt->getBuilder()
            ->issuedBy(Yii::$app->urlManager->createAbsoluteUrl(['/']))// Configures the issuer (iss claim)
            ->permittedFor(Yii::$app->params['frontBaseUrl'])// Configures the audience (aud claim)
            ->identifiedBy('4f1g23a12aa', true)// Configures the id (jti claim), replicating as a header item
            ->issuedAt(time())// Configures the time that the token was issue (iat claim)
            ->expiresAt($time + 3600 * $this->access_expire)// Configures the expiration time of the token (exp claim)
            ->withClaim('uid', $user->id)// Configures a new claim, called "uid"
            ->getToken($signer, $key); // Retrieves the generated token
        $return['refresh_expire'] = $time + 3600 * 24 * $this->refresh_expire;
        $return['refresh_token'] = (string)$jwt->getBuilder()
            ->issuedBy(Yii::$app->urlManager->createAbsoluteUrl(['/']))// Configures the issuer (iss claim)
            ->permittedFor(Yii::$app->params['frontBaseUrl'])// Configures the audience (aud claim)
            ->identifiedBy('4f1g23a12aa', true)// Configures the id (jti claim), replicating as a header item
            ->issuedAt(time())// Configures the time that the token was issue (iat claim)
            ->expiresAt($time + 3600 * 24 * $this->refresh_expire)// Configures the expiration time of the token (exp claim)
            ->withClaim('uid', $user->id)// Configures a new claim, called "uid"
            ->getToken($signer, $key);
        return $return;
    }
}