
REQUIREMENTS
------------

Тестировался на PHP 7.4.


INSTALLATION
------------

### Install via Composer
Выполнить в корневой директории

~~~
$ git clone https://bitbucket.org/JackDavyd/auth_test.git .
$ composer install
~~~

настроить соединение с БД, настроить сервер на корневую директорию web (как обычно для yii) и выполнить

~~~
$ php yii migrate
~~~




### Install from an Archive File

В архиве auth_test.zip собранное приложение. Распаковать в корень, должно завеститсь сразу.


CONFIGURATION
-------------

### Database

В файле `config/db.php` изменить на реальные реквизиты подключения, например:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

API
-------------
#### Регистрация

POST https://auth_test/api/v1/user/auth/signup
~~~
email:aaa@bbb.ru
password:gfhjkm
~~~
Ответ:
````json
{"message":"User registered","code":200}
````
или ошибка валидации в формате
````json
[
    {
        "field": "email",
        "message": "This email address has already been taken."
    }
]
````

#### Подтверждение email

GET https://auth_test/api/v1/user/auth/verify-email?token=423Al1bvzaUgnmGL-jD7NRq_WfR9fCUy_1644045779
~~~
token:423Al1bvzaUgnmGL-jD7NRq_WfR9fCUy_1644045779
~~~
Ответ:
````json
{
  "message": "Email aaa@bbb6.ru verified",
  "user": {
    "id": 7,
    "email": "aaa@bbb6.ru",
    "status": 10,
    "created_at": 1644045780,
    "updated_at": 1644046647,
    "updated_by": null,
    "phone": null,
    "verify_mail": 1,
    "verify_phone": null
  },
  "token": {
    "access_expire": 1644075447,
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXV0aF90ZXN0XC8iLCJhdWQiOiJodHRwOlwvXC9sb2NhbGhvc3Q6MzAwMFwvIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE2NDQwNDY2NDcsImV4cCI6MTY0NDA3NTQ0NywidWlkIjo3fQ.GRsIIqekqLTXgwSUXV_FxBSXkHCVoQz2VZ7vUtN6JHc",
    "refresh_expire": 1646638647,
    "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXV0aF90ZXN0XC8iLCJhdWQiOiJodHRwOlwvXC9sb2NhbGhvc3Q6MzAwMFwvIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE2NDQwNDY2NDcsImV4cCI6MTY0NjYzODY0NywidWlkIjo3fQ.JjpZoEP9yEQ36VnMiX5pU-hglf4KMsth8RrwbY_qdsg"
  }
}
````
Раздел токен на случай если нужна авторизация сразу, от фронтенда зависит.

#### Авторизация

POST https://auth_test/api/v1/user/auth/login
~~~
email:aaa@bbb6.ru
password:gfhjkm
~~~
Ответ:
````json
{
  "user": {
    "id": 7,
    "email": "aaa@bbb6.ru",
    "status": 10,
    "created_at": 1644045780,
    "updated_at": 1644046647,
    "updated_by": null,
    "phone": null,
    "verify_mail": 1,
    "verify_phone": null
  },
  "token": {
    "access_expire": 1644075844,
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXV0aF90ZXN0XC8iLCJhdWQiOiJodHRwOlwvXC9sb2NhbGhvc3Q6MzAwMFwvIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE2NDQwNDcwNDQsImV4cCI6MTY0NDA3NTg0NCwidWlkIjo3fQ.L0MfnhBsY9ESHMSyWYxG1-WlF5ekd5-KsFuI1Iv7qlk",
    "refresh_expire": 1646639044,
    "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXV0aF90ZXN0XC8iLCJhdWQiOiJodHRwOlwvXC9sb2NhbGhvc3Q6MzAwMFwvIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE2NDQwNDcwNDQsImV4cCI6MTY0NjYzOTA0NCwidWlkIjo3fQ.RDL6YeHvds4Vdz6HDCYwjjXzWiCE-XpGEE6XDSZCi90"
  }
}
````
Ошибка в формате
````json
{
  "name": "Not Found",
  "message": "",
  "code": 0,
  "status": 404,
  "type": "yii\\web\\NotFoundHttpException"
}
````
#### Запрос на сброс пароля

POST https://auth_test/api/v1/user/auth/request-password-reset
~~~
email:aaa@bbb4.ru
~~~

Ответ:
````json
{
  "name": "success",
  "message": "Check your email for further instructions."
}
````
Ошибка:
````json
[
  {
    "field": "email",
    "message": "There is no user with this email address."
  }
]
````
#### Сброс пароля
GET https://auth_test/api/v1/user/auth/reset-password?token=n8LCU5YY5KPdli-oyjqEVrdYvZMWKWSy_1643986771

body
~~~
password:gfhjkm1
~~~

Ответ:
````json
{
    "name": "success",
    "message": "New password saved."
}
````

Ошибка:
````json
{
    "name": "Exception",
    "message": "Wrong password reset token.",
    "code": 0,
    "type": "yii\\base\\InvalidArgumentException",
    "file": "/home/jack/www/auth_test/modules/api/modules/v1/modules/user/models/form/ResetPasswordForm.php",
    "line": 57
}
````
Сброс пароля в один запрос. От фронтенда зависит, как там сброс пароля будет организован.
#### Список юзеров
GET https://auth_test/api/v1/user/user/
#### Список активированных юзеров
GET https://auth_test/api/v1/user/user/active-users
#### Юзер по id
GET https://auth_test/api/v1/user/user/1
#### Изменение юзера
PUT https://auth_test/api/v1/user/user/1
~~~
status:10
~~~
#### Удаление юзера
DELETE https://auth_test/api/v1/user/user/1
#### Фильтры
GET https://auth_test/api/v1/user/user/?filter[status]=10
GET https://auth_test/api/v1/user/user/?filter[status]=9