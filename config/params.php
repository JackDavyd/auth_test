<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordMinLength' => 6,
    'user.passwordMaxLength' => 24,
    'user.passwordResetTokenExpire' => 3600,
    'frontBaseUrl' => 'http://localhost:3000/',
    'frontVerifyEmailLink' => 'http://localhost:3000/verify-email/',
    'frontResetPasswordLink' => 'http://localhost:3000/reset-password/'
];
