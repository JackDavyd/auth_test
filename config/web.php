<?php

use yii\web\Response;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$modules = require __DIR__ . '/modules.php';
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'sourceLanguage' => 'en-US',
    'language' => 'ru',
    'bootstrap' => [
        [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ],
        'log',
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@api' => '@app/modules/api/',
        '@v1' => '@api/modules/v1'
    ],
    'modules' => $modules,
    'components' => [
        'jwt' => [
            'class' => \sizeg\jwt\Jwt::class,
            'key' => 'dU2AdcmhZ51ghDbdFkvTxXz5qmW5SYXO',
        ],
        'jwtToken'=>[
            'class' => app\components\JwtToken::class,
        ],
        'request' => [
            'cookieValidationKey' => 'dU2AdcmhZ51ghDbdFkvTxXz5qmW5SYXO',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'multipart/form-data' => 'yii\web\MultipartFormDataParser'
            ],
            'enableCsrfValidation' => true,
            // but if you absolutely need it set cookie domain to localhost
            /*'csrfCookie' => [
                'domain' => 'localhost',
            ],*/
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => \app\modules\api\modules\v1\modules\user\models\User::class,
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule',
                    'controller' => [
                    'api/v1/user/auth',
                    'api/v1/user/user',
                ],
                    'extraPatterns' => [
                        'GET active-user/<id>' => 'active-user',
                        'OPTIONS <module><controller><action>' => 'options'
                    ],
                    'pluralize' => false,
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
        'generators' => [ //自定义生成器
            'rest-model' => [ // generator name
                'class' => 'bestyii\giiRest\generators\model\Generator', // generator class
            ],
            'rest-crud' => [ // generator name
                'class' => 'bestyii\giiRest\generators\crud\Generator', // generator class
            ]
        ],
    ];
}

return $config;
