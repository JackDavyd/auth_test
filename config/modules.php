<?php
return [
    'api' => [
        'class' => 'app\modules\api\Module',
        'modules' => [
            'v1' => [
                'class' => 'app\modules\api\modules\v1\Module',
                'modules' => [
                    'user' => [
                        'class' => 'app\modules\api\modules\v1\modules\user\Module',
                    ],
                ]
            ],
        ]
    ],

];