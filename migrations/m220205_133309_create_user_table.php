<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m220205_133309_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'auth_key' => $this->string(32),
            'type' => $this->smallInteger(2)->defaultValue(1),
            'password_hash' => $this->string(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->defaultValue(null)->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(9),
            'verification_email_token' => $this->string()->unique()->defaultValue(null),
            'verification_phone_token' => $this->smallInteger(6)->unique()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->defaultValue(null),
            'country_code' => $this->string(7)->defaultValue(null),
            'phone_number'=> $this->string(10)->defaultValue(null),
            'verify_mail' => $this->smallInteger(1)->defaultValue(null),
            'verify_phone' => $this->smallInteger(1)->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx_user_phone_unique',
            '{{%user}}',
            ['country_code', 'phone_number'],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
