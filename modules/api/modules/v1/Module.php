<?php

namespace app\modules\api\modules\v1;

/**
 * v1 module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    //public $controllerNamespace = 'app\modules\api\modules\v1\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'user' => [
                // здесь имеет смысл использовать более лаконичное пространство имен
                'class' => 'app\modules\api\modules\v1\modules\user\Module',
            ],
        ];
    }
}
