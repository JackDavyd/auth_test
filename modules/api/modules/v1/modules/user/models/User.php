<?php

namespace app\modules\api\modules\v1\modules\user\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string|null $auth_key
 * @property int|null $type
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property int $status
 * @property string|null $verification_email_token
 * @property int|null $verification_phone_token
 * @property int $created_at
 * @property int $updated_at
 * @property int|null $updated_by
 * @property string $host
 * @property int $host_type
 * @property string|null $country_code
 * @property string|null $phone_number
 * @property int|null $verify_mail
 * @property int|null $verify_phone
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;
    const STATUS_BAN = 11;

    const EMAIL_VERIFY = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED, self::STATUS_BAN]],
            [['type', 'status', 'verification_phone_token', 'created_at', 'updated_at', 'updated_by', 'verify_mail', 'verify_phone'], 'integer'],
            [['email'], 'required'],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'email', 'verification_email_token'], 'string', 'max' => 255],
            [['country_code'], 'string', 'max' => 7],
            [['phone_number'], 'string', 'max' => 10],
            [['email'], 'unique'],
            [['password_reset_token', 'verification_email_token', 'verification_phone_token'], 'unique'],
            [['country_code', 'phone_number'], 'unique', 'targetAttribute' => ['country_code', 'phone_number']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => false,
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'type' => 'Type',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'verification_email_token' => 'Verification Email Token',
            'verification_phone_token' => 'Verification Phone Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'country_code' => 'Country Code',
            'phone_number' => 'Phone Number',
            'verify_mail' => 'Verify Mail',
            'verify_phone' => 'Verify Phone',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $id = $token->getClaim('uid');
        return self::findOne($id);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE, 'verify_mail' => self::EMAIL_VERIFY]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationEmailToken($token)
    {
        return static::findOne([
            'verification_email_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_email_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    /**
     * {@inheritdoc}
     * @return \app\modules\api\modules\v1\modules\user\modules\query\UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\api\modules\v1\modules\user\models\query\UserQuery(get_called_class());
    }

    public function fields()
    {
        $fields = [
            'id',
            'email',
            'status',
            'created_at',
            'updated_at',
            'updated_by',
            'phone' => function () {
                return !empty($this->country_code) && !empty($this->phone_number) ? "+{$this->country_code} {$this->phone_number}" : null;
            },
            'verify_mail',
            'verify_phone'
        ];
        return $fields;
    }
}
