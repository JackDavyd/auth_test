<?php

namespace app\modules\api\modules\v1\modules\user\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\api\modules\v1\modules\user\models\User;

/**
 * UserSearch represents the model behind the search form of `app\models\user\User`.
 */
class UserSearch extends User
{
    public $created_at_from;
    public $created_at_to;
    public $created_at_from_search;
    public $created_at_to_search;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            ['email', 'email'],
            [['auth_key', 'password_hash', 'password_reset_token', 'email', 'verification_email_token'], 'string'],
            ['created_at_from', 'date', 'timestampAttribute' => 'created_at_from_search', 'format' => 'php:d.m.Y'],
            ['created_at_to', 'date', 'timestampAttribute' => 'created_at_to_search', 'format' => 'php:d.m.Y']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['between', 'created_at', $this->created_at_from_search, $this->created_at_to_search]);

        return $dataProvider;
    }

    public function formName()
    {
        return 'filter';
    }
}
