<?php
namespace app\modules\api\modules\v1\modules\user\models\form;

use Yii;
use yii\base\Model;
use app\modules\api\modules\v1\modules\user\models\User;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $country_code;
    public $phone_number;
    /** @var $type integer type signup. 1 by email or 2 by phone */
    public $type = 1;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password', 'country_code', 'phone_number'], 'trim'],
            [
                'password', 'required',
                'message' => 'Either password is required.',
                'when' => function ($model) {
                    return !empty($model->email);
                }
            ],
            [
                'email', 'required',
                'message' => 'Either email or phone is required.',
                'when' => function ($model) {
                    return empty($model->country_code) || empty($model->phone_number);
                }
            ],
            [
                ['country_code', 'phone_number'], 'required',
                'message' => 'Either email or phone is required.',
                'when' => function ($model) {
                    return empty($model->email);
                }
            ],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::class,
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength'], 'max' => Yii::$app->params['user.passwordMaxLength']],
            ['type', 'integer'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
