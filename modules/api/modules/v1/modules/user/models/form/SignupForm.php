<?php
namespace app\modules\api\modules\v1\modules\user\models\form;

use app\modules\api\modules\v1\modules\user\events\SignupEvents;
use Yii;
use yii\base\BaseObject;
use yii\base\Event;
use yii\base\Model;
use app\modules\api\modules\v1\modules\user\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $country_code;
    public $phone_number;
    /** @var $type integer type signup. 1 by email or 2 by phone */
    public $type = 1;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password', 'country_code', 'phone_number'], 'trim'],
            [
                'password', 'required',
                'message' => 'Either password is required.',
                'when' => function($model) { return !empty($model->email); }
            ],
            [
                'email', 'required',
                'message' => 'Either email or phone is required.',
                'when' => function($model) { return empty($model->country_code) || empty($model->phone_number); }
            ],
            [
                ['country_code', 'phone_number'], 'required',
                'message' => 'Either email or phone is required.',
                'when' => function($model) { return empty($model->email); }
            ],
            ['email', 'email'],
            ['country_code', 'string', 'max' => 7],
            ['phone_number', 'string', 'max' => 10],

            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::class, 'message' => Yii::t('yii','This email address has already been taken.')],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength'], 'max' => Yii::$app->params['user.passwordMaxLength']],
            ['type', 'integer']
        ];
    }


    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }
        $user = new User();
        if($this->email){
            $user->email = $this->email;
            $user->generateEmailVerificationToken();
        }
        if(!empty($model->country_code) && !empty($model->phone_number)){
            $user->country_code = $this->country_code;
            $user->phone_number = $this->phone_number;
            $this->type = 2;
        }
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user = call_user_func_array([SignupEvents::class, 'beforeSignup'], ['args'=>['form'=>$this, 'user'=>$user]]);
        if($user->save()){
            call_user_func_array([SignupEvents::class, 'afterSignup'],  ['args'=>['form'=>$this, 'user'=>$user]]);
            return true;
        }
        return false;
    }
}
