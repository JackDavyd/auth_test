<?php

namespace app\modules\api\modules\v1\modules\user\models\form;

use app\modules\api\modules\v1\modules\user\models\User;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

class VerifyEmailForm extends Model
{
    /**
     * @var string
     */
    public $token;

    public function rules()
    {
        return [
            ['token', 'string'],
        ];
    }

    /**
     * Verify email
     *
     * @return User|null the saved model or null if saving fails
     */
    public function verifyEmail()
    {
        if(!$this->validate()){
            return null;
        }
        $user = User::findByVerificationEmailToken($this->token);
        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }
        $user->status = User::STATUS_ACTIVE;
        $user->verify_mail = User::EMAIL_VERIFY;
        return $user->save() ? $user : null;
    }
}
