<?php
namespace app\modules\api\modules\v1\modules\user\models\form;

use yii\base\InvalidArgumentException;
use yii\base\Model;
use Yii;
use app\modules\api\modules\v1\modules\user\models\User;
use yii\web\NotFoundHttpException;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;
    public $password_reset_token;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength'], 'max' => Yii::$app->params['user.passwordMaxLength']],
            ['password_reset_token', 'exist',
                'targetClass' => User::class,
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this password_reset_token.'
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        if(!$this->validate()){
            return false;
        }
        $user = User::findByPasswordResetToken($this->password_reset_token);
        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        $user->generateAuthKey();

        return $user->save(false);
    }
}
