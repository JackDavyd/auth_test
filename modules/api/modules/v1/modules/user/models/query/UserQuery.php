<?php

namespace app\modules\api\modules\v1\modules\user\models\query;

use app\modules\api\modules\v1\modules\user\models\User;

/**
 * This is the ActiveQuery class for [[\app\modules\api\modules\v1\modules\user\modules\User]].
 *
 * @see \app\modules\api\modules\v1\modules\user\models\User
 */
class UserQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('status = ' . User::STATUS_ACTIVE);
    }

    public function findByEmail($email)
    {
        return $this->andWhere(['email' => $email]);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\api\modules\v1\modules\user\models\User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\api\modules\v1\modules\user\models\User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
