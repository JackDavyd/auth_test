<?php

namespace app\modules\api\modules\v1\modules\user\controllers;

use Yii;
use app\modules\api\modules\v1\modules\user\models\form\LoginForm;
use app\modules\api\modules\v1\modules\user\models\form\SignupForm;
use app\modules\api\modules\v1\modules\user\models\form\VerifyEmailForm;
use app\modules\api\modules\v1\modules\user\models\form\ResetPasswordForm;
use app\modules\api\modules\v1\modules\user\models\form\PasswordResetRequestForm;
use app\modules\api\modules\v1\modules\user\models\User;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;


/**
 * Default controller for the `v1` module
 */
class AuthController extends ActiveController
{
    public $modelClass = User::class;


    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        return $actions;
    }

    protected function verbs()
    {
        return [
            'signup' => ['POST'],
            'validate' => ['POST'],
            'verify-email' => ['GET'],
            'login' => ['POST'],
            'request-password-reset' => ['POST'],
            'reset-password' => ['GET'],
        ];
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];

        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'options',
                'signup',
                'validate',
                'verify-email',
                'login',
                'request-password-reset',
                'reset-password',
            ],
        ];
        return $behaviors;
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        $model->load(Yii::$app->request->post(), '');
        return $model->signup() ? ['message' => 'User registered', 'code' => 200] : $model;
    }

    public function actionValidate()
    {
        $type = \Yii::$app->request->post('type');
        $model = new SignupForm();
        $model->load(Yii::$app->request->post(), '');
        return $model->validate($type) ? true : $model->getErrors($type);
    }

    public function actionVerifyEmail($token)
    {
        $model = new VerifyEmailForm();
        $model->load(['token' => $token], '');
        $user = $model->verifyEmail();
        if ($user instanceof User) {
            $token = Yii::$app->jwtToken->createToken($user);
            return [
                'message' => "Email $user->email verified",
                'user' => $user,
                'token' => $token,
            ];
        }
        return new BadRequestHttpException('Sorry, we are unable to verify your account with provided token.');
    }

    public function actionLogin()
    {

        $model = new LoginForm();
        $model->load(Yii::$app->request->post(), '');
        if ($model->login()) {
            $user = !empty(Yii::$app->user->identity) ? Yii::$app->user->identity : null;
            if ($user) {
                $token = Yii::$app->jwtToken->createToken($user);
                return [
                    'user' => $user,
                    'token' => $token,
                ];
            }
        }
        return $model;
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        $model->load(Yii::$app->request->post(), '');
        return $model->sendEmail() ? ['name' => 'success', 'message' => 'Check your email for further instructions.'] : $model;
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $post = Yii::$app->request->post();
        $data = array_merge($post, ['password_reset_token'=>$token]);
        $model = new ResetPasswordForm();
        $model->load($data,'');
        return $model->resetPassword() ? ['name' => 'success', 'message' => 'New password saved.'] : $model;

    }
}
