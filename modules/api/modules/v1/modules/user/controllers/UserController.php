<?php

namespace app\modules\api\modules\v1\modules\user\controllers;

use Yii;

use app\modules\api\modules\v1\modules\user\models\User;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\filters\Cors;
use yii\rest\ActiveController;
use app\modules\api\modules\v1\modules\user\models\search\UserSearch;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `v1` module
 */
class UserController extends ActiveController
{
    public $modelClass = User::class;
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items'
    ];

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs = array_merge($verbs,
            [
                'active-user'=>['GET', 'HEAD'],
                'active-users'=>['GET', 'HEAD']
            ]);
        return $verbs;
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];

        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [],
        ];
        return $behaviors;
    }

    public function prepareDataProvider() {

        $searchModel = new UserSearch();
        return $searchModel->search(\Yii::$app->request->queryParams);
    }

    public function actionActiveUsers(){
        return new ActiveDataProvider([
            'query' => User::find()->active(),
            'pagination' => [
                'pageSize' => 10
            ]
        ]);
    }

    public function actionActiveUser($id){
        return User::findOne(['id'=>(int)$id, 'status' => User::STATUS_ACTIVE]);
    }
}
