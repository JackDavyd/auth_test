<?php

namespace app\modules\api\modules\v1\modules\user\events;

class SignupEvents
{
    public function afterSignup($args)
    {
        $user = $args['user'];
        $form = $args['form'];
        if($form->type == 1){
            \Yii::$app->mailer->setViewPath("@v1/modules/user/mail/");
            return \Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                    ['user' => $user, 'form' => $form]
                )
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name])
                ->setTo($user->email)
                ->setSubject('Регистрация на auth-test')
                ->send();
        }
    }

    public function beforeSignup($args){
        $user = $args['user'];
        $form = $args['form'];
        if((int)$form->type === 2){
            //@TODO Добавить получение токена по телефону.
            $user->verification_phone_token = 12345;
        }
        return $user;
    }
}