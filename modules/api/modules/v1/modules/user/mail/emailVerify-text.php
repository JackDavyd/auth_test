<?php

/* @var $this yii\web\View */
/* @var $user \app\modules\api\modules\v1\modules\user\models\User */

$url = Yii::$app->params['frontVerifyEmailLink'];
$verifyLink = "{$url}?token={$user->verification_email_token}";
?>
Hello <?= $user->email?>,

Follow the link below to verify your email:

<?= $verifyLink ?>
