<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */
$url = \Yii::$app->params['frontResetPasswordLink'];
$resetLink = "{$url}?token=" . $user->password_reset_token;

?>
Hello <?= $user->email ?>,

Follow the link below to reset your password:

<?= $resetLink ?>
