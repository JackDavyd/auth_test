<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \app\modules\api\modules\v1\modules\user\models\User */

$url = Yii::$app->params['frontVerifyEmailLink'];
$verifyLink = "{$url}?token={$user->verification_email_token}";
?>
<div class="verify-email">
    <p>Hello <?= Html::encode($user->email) ?>,</p>
    <p>Follow the link below to verify your email:</p>
    <p><?= Html::a(Html::encode($verifyLink), $verifyLink) ?></p>
</div>
