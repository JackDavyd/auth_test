<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$url = \Yii::$app->params['frontResetPasswordLink'];
$resetLink = "{$url}?token=" . $user->password_reset_token;
?>
<p>Hello <?= Html::encode($user->email) ?>,</p>
<p>Follow the link below to reset password:</p>
<p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>

</div>
